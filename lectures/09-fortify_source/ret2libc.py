#!/usr/bin/env python3

import sys

payload = b'A' * 8

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()

sys.stdout.buffer.write(payload)


