#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static char mybuf[4096];

void oops(void)
{

puts("           |~\n           |.---.\n          .'_____`. /\\\n          |~xxxxx~| ||\n          |_  #  _| ||\n     .------`-#-'-----.\n    (___|\\_________/|_.`.\n     /  | _________ | | |\n    /   |/   _|_   \\| | |\n   /   /X|  __|__  |/ `.|\n  (  --< \\\\/    _\\//|_ |`.\n  `.    ~----.-~=====,:=======\n    ~-._____/___:__(``/| |\n      |    |      XX|~ | |\n       \\__/======| /|  `.|\n       |_\\|\\    /|/_|    )\n       |_   \\__/   _| .-'\n       | \\ .'||`. / |(_|\n    LS |  ||.'`.||  |   )\n       |  `'|  |`'  |  /\n       |    |  |    |\\/\n ");
exit (-1);
}


void echo_input(char* arg)
{
	char buf[256];

	strcpy(buf, arg);
	puts(buf);
}

void read_input(FILE *fp, char *buf, size_t len)
{
	int l;
	l = fread(buf, len, 1, fp);
	if (l < 0) {
		perror("error reading input");
		exit (1);
	}
}

int main(int argc, char* argv[]) 
{
	read_input(stdin, mybuf, sizeof(mybuf));

	echo_input(mybuf);

	return 0;
}
