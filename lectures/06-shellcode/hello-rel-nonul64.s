# ----------------------------------------------------------------------------------------
# Writes "Hello, World" to the console using only system calls.
# Runs on 64-bit Linux only.
# ----------------------------------------------------------------------------------------

        .global _start

        .text
_start:
        # write(1, message, 11)
	xor	%eax, %eax
	inc	%eax			# system call 1 is write
	mov	%eax, %edi 		# file handle 1 is stdout
	jmp	load_message
message_loaded:
	pop	%rsi
	xor	%edx, %edx
	mov	$11, %dl		# number of bytes
        syscall                         # invoke operating system to do the write

        # exit(0)
	xor	%eax, %eax
	mov	$60, %al		# system call 60 is exit
        xor     %edi, %edi              # we want return code 0
        syscall                         # invoke operating system to exit
load_message:
	call message_loaded
        .ascii  "Hello world"
