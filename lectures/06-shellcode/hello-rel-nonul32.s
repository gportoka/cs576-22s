# ----------------------------------------------------------------------------------------
# Writes "Hello, World" to the console using only system calls.
# Runs on 32-bit Linux only.
# ----------------------------------------------------------------------------------------

        .global _start

        .text
_start:
        # write(1, message, 11)
	xor	%eax, %eax
        mov     $4, %al                 # system call 4 is write
	xor 	%ebx, %ebx
        mov     $1, %bl                 # file handle 1 is stdout
	jmp	load_message_gadget
load_message:
	pop	%ecx
	xor	%edx, %edx
        mov     $11, %dl		# number of bytes
        int	$0x80                   # invoke operating system to do the write

        # exit(0)
	xor	%eax, %eax
	inc	%eax			# system call 1 is exit
        xor     %ebx, %ebx              # we want return code 0
        int	$0x80                   # invoke operating system to exit
load_message_gadget:
	call load_message
        .ascii  "Hello world"
