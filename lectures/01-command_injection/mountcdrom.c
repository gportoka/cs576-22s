#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
	char cmd[1024];
	char *devnum;
	int r;

	if (argc > 1) {
		devnum = argv[1];
	} else {
		devnum = "0";
	}

	setuid(0);

	sprintf(cmd, "mount /dev/sr%s /media/cdrom%s", devnum, devnum);
	printf("Running %s\n", cmd);
	
	r = system(cmd);
	if (r < 0) {
		perror("Could not launch command");
	} else if (r == 0) {
		puts("Command terminated successfully");
	} else {
		puts("Command terminated with error");
	}

	return (0);
}
