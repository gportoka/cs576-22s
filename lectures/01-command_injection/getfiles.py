#!/usr/bin/env python3

import sys
import os

if len(sys.argv) > 1:
    directory = sys.argv[1]
else:
    directory = "."

ret = os.system("ls " + directory)

if ret:
    status = "with error"
else:
    status = "successfully"

print (">> Program finished %s" % status)  
