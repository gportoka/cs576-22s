#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int g_constant = 66;

/* Function with no parameters or local vars */
void func0p0l(void)
{
	g_constant = 0;
}

/* Function with no parameters but with local vars */
void func0p3l(void)
{
	int a;
	char buf[16];
	unsigned long l;

	l = g_constant * 2;
	a = l * 3;
	g_constant = a;
}


/* Function with parameters and local vars */
/* Function returns value */
unsigned long func2p1lr(int a, int b)
{
	unsigned long l;

	l = a + b + g_constant;
	g_constant = l;
	return l;
}


/* Function with parameters and local vars */
/* Function possible spills register */
void func2p1ls(int a, int b)
{
	char buf[16];

	snprintf(buf, sizeof(buf), "%g", (double)a / b);
	buf[sizeof(buf) - 1] = '\0';
	printf("%s\n", buf);
}


/*
 * run s|g|h string
 */
int main(int argc, char **argv)
{
	func0p0l();
	func0p3l();
	func2p1lr(100, 200);
	func2p1ls(567, 789);

	return 0;
}
