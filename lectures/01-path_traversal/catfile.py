#!/usr/bin/env python3

import sys
import os

if len(sys.argv) > 1:
    directory = sys.argv[1]
else:
    directory = "null.txt"

home = os.path.expanduser('~')
fn = home + "/" + directory

print("Sending file: " + fn)
ret = os.system("cat " + fn)

if ret:
    status = "with error"
else:
    status = "successfully"

print (">> Program finished %s" % status)  
