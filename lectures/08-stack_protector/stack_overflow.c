#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static char mybuf[4096];

void surprise(void)
{
	puts("           |~\n           |.---.\n          .'_____`. /\\\n          |~xxxxx~| ||\n          |_  #  _| ||\n     .------`-#-'-----.\n    (___|\\_________/|_.`.\n     /  | _________ | | |\n    /   |/   _|_   \\| | |\n   /   /X|  __|__  |/ `.|\n  (  --< \\\\/    _\\//|_ |`.\n  `.    ~----.-~=====,:=======\n    ~-._____/___:__(``/| |\n      |    |      XX|~ | |\n       \\__/======| /|  `.|\n       |_\\|\\    /|/_|    )\n       |_   \\__/   _| .-'\n       | \\ .'||`. / |(_|\n    LS |  ||.'`.||  |   )\n       |  `'|  |`'  |  /\n       |    |  |    |\\/\n ");
}

void oops(void)
{	
	surprise();
	exit (-1);
}

int run_prog(char * const bin)
{
	return execlp(bin, bin, NULL);
}

void echo_input(const char *data, size_t len)
{
	char buf[256];

	memcpy(buf, data, len);
	puts(buf);
}

void read_input(FILE *fp, char *buf, size_t len)
{
	if (fread(buf, len, 1, fp) < 0) {
		perror("error reading input");
		exit (1);
	}
}

/*
 * expected input
 * echo: length_of_string string
 * output:
 * string
 */
int main(int argc, char* argv[]) 
{
	size_t bytes;
	char *data;

	read_input(stdin, mybuf, sizeof(mybuf));

	if (strncmp(mybuf, "echo: ", 6) != 0)
		return 0;

	bytes = (size_t)strtol(mybuf + 6, &data, 10);

	data++;
	data[bytes] = '\0';
	echo_input(data, bytes + 1);

	return 0;
}
