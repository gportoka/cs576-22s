# Movable code for loading a code address on edx and calling the exit system call
        .global _start

        .text
_start:
	call L3
L3:
	pop %edx			# Loads L3
	add $(L1-L3), %edx			# Adjusts edx to point to L1
L1:
	mov	$1, %eax
        xor     %ebx, %ebx              # we want return code 0
        int 	$0x80                  # invoke operating system to exit
