# Unmovable code for loading a code address on edx and calling the exit system call
        .global _start

        .text
_start:
	mov	 $(L1), %edx		# Load L1 on edx
L1:
	mov	$1, %eax
        xor     %ebx, %ebx            	# we want return code 0
        int 	$0x80                 	# invoke operating system to exit
