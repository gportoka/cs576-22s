# Movable code for loading a code address on rdx and calling the exit system call
        .global _start

        .text
_start:
	mov L1(%rip), %rdx		# Loads L1 to rdx
L1:
	mov	$60, %eax
        xor     %edi, %edi              # we want return code 0
        syscall                         # invoke operating system to exit
