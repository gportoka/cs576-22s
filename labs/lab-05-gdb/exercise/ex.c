#include <stdio.h>
#include <string.h>

void f1(const char *str)
{
	char buf1[16];

	strcpy(buf1, str);

	puts(buf1);
}

int main(int argc, char **argv)
{
	if (argc < 2)
		return 1;
	f1(argv[1]);
	return 0;
}
