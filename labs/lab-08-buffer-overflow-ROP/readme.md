# Heap Overflow Examples

## Additional tools

Checkout repo https://github.com/JonathanSalwan/ROPgadget for collecting gadgets.

## Running program

Use the setarch utility to run the programs. 

Example:
```
$ setarch -R ./vuln
```

## Process layout outside gdb

GDB can alter the layout of the program (stack). To learn its actual addresses,
you can run the program at the command line and attach to it wit GDB using the
`attach <pid>` command.
