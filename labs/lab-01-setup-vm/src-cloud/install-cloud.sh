#!/bin/bash

# Set the USERID shell variable.
USERID=seed

#================================================
echo "Installing software for the cloud VM ..."

# Instal a light-weighted window manager.
# It will ask us to choose a default display manager, chose LightDM. 
sudo apt -y install xfce4 xfce4-goodies

# Install TigerVNC server
sudo apt -y install tigervnc-standalone-server tigervnc-xorg-extension


echo "Customization ..."

HOMEDIR=/home/$USERID

# Copy the desktop image files
sudo cp -f Files/System/Background/* /usr/share/backgrounds/xfce/

# Configure the VNC server 
sudo -u $USERID mkdir -p $HOMEDIR/.vnc
sudo -u $USERID cp Files/System/vnc_xstartup $HOMEDIR/.vnc/xstartup
sudo -u $USERID chmod u+x $HOMEDIR/.vnc/xstartup

# Create launcher icons on the desktop
sudo -u $USERID mkdir -p $HOMEDIR/Desktop
sudo -u $USERID cp Files/System/Desktop/*  $HOMEDIR/Desktop
sudo -u $USERID chmod u+x $HOMEDIR/Desktop/*.desktop
sudo -u $USERID mkdir -p $HOMEDIR/.local/icons
sudo -u $USERID cp Files/System/Icons/*  $HOMEDIR/.local/icons


